import discord
import asyncio

from ..client import BotClient
from ..message.com import ControlledMessage
from ..message.plain import EphemeralMessage
from ..message.prompt import InteractivePrompt

class PollResponsePrompt(InteractivePrompt):
	@staticmethod
	async def instantiate(client: BotClient, message: discord.Message, tag: int):
		return await pollResponsePrompt(
			client, message.channel, tag, message
		)

	def __init__(self, client: BotClient, channel: discord.Channel, model):
		super().__init__(client, channel)
		self.model = model

	def sweepOnStart(self):
		return True

	def header(self):
		headerForResponders = {
			"public" :      lambda: "Public Poll",
			"community" :   lambda: "Community Poll",
			"server" :      lambda: "Server Poll   " + "`" + self.model["server"] + "`",
			"channel" :     lambda: "Channel Poll   " + "`#" + self.model["channel"] + "`",
			"group" :       lambda: "Group Poll   " + "`" + self.model["role"] + "`"
		}
		return headerForResponders[self.model["responders"]]()

	def tag(self):
		return self.model["pollId"]

	def body(self):
		return "🔸 " + self.model["question"]

	def block(self):
		return self.model["description"]

	def footer(self):
		return "Click below to cast your vote."

	async def handle(self, reaction, user):
		print("Vote cast")

class MultipleChoicePrompt(PollResponsePrompt):
	def block(self):
		numOptions = len(self.model["options"])

		block  = ""
		for n in range(numOptions):
			option = self.model["options"][n]
			block += str(n + 1) + ")  " + option
			if n != (numOptions - 1):
				block += "\n"
		return block

	def buttons(self):
		numOptions = len(self.model["options"])
		numbers = range(1, numOptions + 1)
		return [str(num) for num in numbers]

class ShortResponsePrompt(PollResponsePrompt):
	def footer(self):
		return "Type `~submit` with your response, or `@mention` me."

class ScaleRatingPrompt(PollResponsePrompt):
	def buttons(self):
		start = int(self.model["rangeStart"])
		stop = int(self.model["rangeStop"])
		numbers = range(start, stop + 1)
		return [str(num) for num in numbers]

class YesOrNoPrompt(PollResponsePrompt):
	def buttons(self):
		return ["no", "yes"]

class OnlineFormPrompt(PollResponsePrompt):
	def footer(self):
		return "Click the pencil & paper to get your voting link."
	
	def buttons(self):
		return ["form"]

async def pollResponsePrompt(client: BotClient, channel: discord.Channel,
	tag: int, existingMessage: discord.Message = None):

	model = pollModel(tag)

	promptForPollType = {
		"multipleChoice" : MultipleChoicePrompt,
		"shortResponse" : ShortResponsePrompt,
		"scaleRating" : ScaleRatingPrompt,
		"yesOrNo" : YesOrNoPrompt,
		"onlineForm" : OnlineFormPrompt
	}

	PromptType = promptForPollType[model["type"]]
	prompt = PromptType(client, channel, model)
	if existingMessage != None:
		prompt.message = existingMessage
	
	return prompt

def pollModel(pollId):
	return {
		"pollId" : pollId,
		"responders" : "community", # public, community, server, channel, group
		"server": "The Michael Brooks Show",
		"channel": "tmbs-live",
		"role": "book-nerds",
		"type": "onlineForm", # multipleChoice, shortResponse, yesOrNo, scaleRating, onlineForm
		"question": "2018 Pet Election",
		#"description": None,
		"description": "Submit your photos online.",
		"options": [
			"To make the world a better place",
			"Attention seeking behavior",
			"To prove my father wrong",
			"In order to obtain vast amounts of money",
			"So that I could shove it in your face"
		],
		"rangeStart": 1,
		"rangeStop": 5
	}