import discord

from ..client import BotClient
from ..message.plain import PlainMessage
from ..message.prompt import MultiPagePrompt

responderTypes = [
	("server", 		"Server     All members of this server"),
	("channel", 	"Channel    People in a specific channel"),
	("group", 		"Group      Users with a specific role"),
	("community",	"Community  Members of any community server"),
	("public", 		"Public     Anyone, browser only")
]

pollTypes = [
	("yesOrNo",			"Yes or No"),
	("multipleChoice",	"Multiple Choice"),
	("scaleRating",		"Rating on a Scale"),
	("shortResponse",	"Short Response"),
	("internetForm",	"Internet Form")
]

scaleTypes = [
	((1, 5),	"1 - 5"),
	((1, 10),	"1 - 10"),
	((0, 10),	"0 - 10")
]

class CreatePollWizard(MultiPagePrompt):
	def __init__(self, client: BotClient, channel: discord.Channel):
		super().__init__(client, channel)
		self.pollModel = {}

	def firstPage(self):
		return self.pageResponders()

	def header(self):
		return "Create New Poll" 

	def tag(self):
		return self.nextPollId()

	def keywords(self):
		return ["~"]

	def pageResponders(self):
		body = "🔹 Responders"
		block = multiChoiceDescription(responderTypes)
		(buttons, actions) = multiChoiceButtons(responderTypes, self.respondersSelected)

		buttons = ["back"] + buttons

		return (body, block, None, buttons, actions)

	async def respondersSelected(self, choice):
		self.pollModel["responders"] = multiChoiceSelection(responderTypes, choice)

		if self.pollModel["responders"] == "public":
			# Do action for creating a public poll
			# ...
			await self.update(self.pageContinueOnline())
		else:
			await self.update(self.pagePollType())

	def pageContinueOnline(self):
		body = "🔹 Responders > Poll Type > Continue Online"
		block = "You should receive a link as a private message."

		actions = {}
		buttons = ["back"]
		actions["back"] = lambda: self.backSelected(self.pageResponders)

		return (body, block, None, buttons, actions)

	def pagePollType(self):
		body = "🔹 Responders > Poll Type"
		block = multiChoiceDescription(pollTypes)
		(buttons, actions) = multiChoiceButtons(pollTypes, self.pollTypeSelected)

		buttons = ["back"] + buttons
		actions["back"] = lambda: self.backSelected(self.pageResponders)

		return (body, block, None, buttons, actions)

	async def pollTypeSelected(self, choice):
		pollType = multiChoiceSelection(pollTypes, choice)
		self.pollModel["type"] = pollType

		if pollType == "internetForm":
			# Do action for creating internet form
			# ...
			await self.update(self.pageContinueOnline())
		else:
			if pollType == "multipleChoice":
				self.pollModel["options"] = []
			await self.update(self.pageQuestion())

	def pageQuestion(self):
		body = "🔹 Responders > Poll Type > Question"
		block = "What is the poll's question or title?"
		footer = "Type `~` followed by some text, and press enter."

		reactions = ["back"]
		actions = {}
		actions["back"] = lambda: self.backSelected(self.pagePollType)
		actions["~"] = lambda x: self.pollQuestionProvided(x)

		return (body, block, footer, reactions, actions)

	async def pollQuestionProvided(self, words):
		self.pollModel["question"] = " ".join(words)
		if self.pollModel["type"] == "multipleChoice":
			await self.update(self.pageOptions())
		else:
			await self.update(self.pageDescription())

	def blockForOptions(self, options):
		block = "Options appear here as entered.\n"
		block += "‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾\n"
		count = len(options)
		for n in range(count):
			block += str(n + 1) + ")  " + options[n] + "\n"
		block += str(count + 1) + ")  ________"
		if count < 9:
			block += "\n ...\n"

		return block

	def pageOptions(self):
		body = "🔹 Responders > Poll Type > Question > Options"
		block = self.blockForOptions(self.pollModel["options"])
		footer = "Type `~` followed by some text, and press enter."

		reactions = ["back", "forward"]
		actions = {}
		actions["back"] = lambda: self.backSelected(self.pageQuestion())
		actions["forward"] = lambda: self.optionsComplete()
		actions["~"] = lambda x: self.optionProvided(x)

		return (body, block, footer, reactions, actions)

	async def optionProvided(self, words):
		self.pollModel["options"].append(" ".join(words))
		return await self.update(self.pageOptions())

	async def optionsComplete(self):
		return await self.update(self.pageReview())

	def pageDescription(self):
		body = "🔹 Responders > Poll Type > Question > Description"
		block  = "An optional description can be provided for the poll. It will be shown in a text box like this one."
		footer = "Type `~` followed by some text, and press enter."

		reactions = ["back", "forward"]
		actions = {}
		actions["back"] = lambda: self.backSelected(self.pageQuestion())
		actions["forward"] = lambda: self.descriptionProvided(None)
		actions["~"] = lambda x: self.descriptionProvided(x)

		return (body, block, footer, reactions, actions)

	async def descriptionProvided(self, words):
		if words != None:
			self.pollModel["description"] = " ".join(words)
		await self.update(self.pageReview())

	def pageReview(self):
		body = "🔹 Responders > Poll Type > Question > Description > Review"
		block  = "Responders: " + self.pollModel["responders"]
		block += "Type: " + self.pollModel["type"]
		block += "etc..."
		footer = "Confirm poll creation?"

		reactions = ["back", "accept"]
		actions = {}
		actions["back"] = lambda: self.backSelected(self.pageDescription())
		actions["accept"] = lambda: self.reviewConfirmed()
		
		return (body, block, footer, reactions, actions)

	async def reviewConfirmed(self):
		self.dismiss()
		# Do thing to create poll...
		tag = str(self.tag())
		message = "\n✅ Poll **#" + tag + "** has been created."
		message += " Use `~show " + tag + "` to display it.\n\n"
		message += '"' + self.pollModel["question"] + '"'
		await PlainMessage(self.client, self.channel, message).present()

	async def backSelected(self, previousPage):
		await self.update(previousPage())
	
	def nextPollId(self):
		return 312

def multiChoiceDescription(descriptors):
	block = ""
	for n in range(len(descriptors)):
		block += str(n + 1) + ")  " + descriptors[n][1]
		if n != len(descriptors) - 1:
			block += "\n"
	return block

def multiChoiceButtons(descriptors, handler):
	buttons = [str(n + 1) for n in range(len(descriptors))]
	actions = {}

	# Ensures final value for n, to work around python's late-binding lambda
	def handlerCall(n): return lambda: handler(n)
	for n in range (len(descriptors)):
		actions[str(n + 1)] = handlerCall(n)
	
	return (buttons, actions)

def multiChoiceSelection(descriptors, n):
	return descriptors[n][0]
