import discord

from .client import BotClient
from .message.prompt import MultiPagePrompt
from .message.plain import EphemeralMessage
from .poll.create import CreatePollWizard

class MainMenu(MultiPagePrompt):
	def firstPage(self):
		return self.pageOne()

	def header(self):
		return "Main Menu"

	def keywords(self):
		return None

	def pageOne(self):
		body = None

		block  = "1)  Post to Community Site   ~post\n"
		block += "2)  Join a Server as Guest   ~join\n"
		block += "3)     Create New Poll       ~poll\n"
		block += "4)      Show Poll Form       ~show #\n"
		block += "5)       Poll Results        ~result #"

		footer = "Page 1 of 2"

		reactions = ["cancel", "back", "1", "2", "3", "4", "5", "forward"]

		def makeLambda(x):
			return lambda: self.menuItemSelected(1, x)
		actions = {}
		actions["forward"] = lambda: self.navSelected(self.pageTwo)
		for n in range(1, 6):
			actions[str(n)] = makeLambda(n)

		return (body, block, footer, reactions, actions)

	def pageTwo(self):
		body = None

		block  = "1)      Schedule Message     ~schedule\n"
		block += "2)       Configure Bot       ~config\n"
		block += "3)        Restart Bot        ~boom\n"
		
		footer = "Page 2 of 2"

		reactions = ["cancel", "back", "1", "2", "3", "4", "5", "forward"]

		actions = {}
		actions["back"] = lambda: self.navSelected(self.pageOne)
		for n in range(1, 6):
			actions[str(n)] = (lambda: self.menuItemSelected(2, n))

		return (body, block, footer, reactions, actions)

	async def menuItemSelected(self, page, choice):
		if page == 1 and choice == 3:
			self.dismiss()
			wizard = CreatePollWizard(self.client, self.channel)
			await wizard.present()
		else:
			await EphemeralMessage(self.client, self.channel, "Not implemented yet").present()
		
	async def navSelected(self, nextPage):
		await self.update(nextPage())
