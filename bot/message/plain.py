import sys
import os
import discord
import asyncio

from ..client import BotClient
from .com import ControlledMessage

class PlainMessage(ControlledMessage):
	def __init__(self, client: BotClient, channel: discord.Channel,
		text: str, user: discord.User = None):
		super().__init__(client, channel)

		self.text = text
		if user != None:
			self.text = user.mention + " " + self.text

	def body(self):
		return self.text

class EphemeralMessage(PlainMessage):
	def sweepOnStart(self):
		return True

	async def present(self):
		await super().present()
		self.client.runCoroutine(self.removeMessage)

	async def removeMessage(self):
		await asyncio.sleep(3.0)
		await self.client.delete_message(self.message)

class ErrorMessage(ControlledMessage):
	@staticmethod
	async def instantiate(client: BotClient, message: discord.Message, tag: int):
		msg = ErrorMessage(client, message.channel, None)
		msg.message = message
		async def update():
			await msg.appRestarted()
		client.runCoroutine(update)

	def __init__(self, client: BotClient, channel: discord.Channel, ex: str = None):
		super().__init__(client, channel)

		self.messageText = "\n⚠️ The program generated an exception and must restart.\n"
		self.messageBlock = ex

	async def appRestarted(self):
		self.messageText = "\n🔄 The program restarted.\n\n"
		await self.update()
		await asyncio.sleep(3.5)
		await self.client.delete_message(self.message)

	def reinstantiateOnStart(self):
		return True

	def body(self):
		return self.messageText

	def block(self):
		return self.messageBlock

	