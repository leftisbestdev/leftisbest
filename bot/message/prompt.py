from discord import Channel, User
import asyncio

from ..client import BotClient
from .com import ControlledMessage
from .plain import EphemeralMessage

reactions = [
	("❌", "cancel"), ("✅", "accept"),
	("👈", "back"), ("👉", "forward"),
	("👍", "yes"), ("👎", "no"), 
	("0⃣", "0"),
	("1⃣", "1"), ("2⃣", "2"), ("3⃣", "3"), ("4⃣", "4"), ("5⃣", "5"),
	("6⃣", "6"), ("7⃣", "7"), ("8⃣", "8"), ("9⃣", "9"), ("🔟", "10"),
	("📝", "form")
]

def reactionEmoji(name):
	for reaction in reactions:
		if reaction[1] == name:
			return reaction[0]
	return None

def reactionName(emoji):
	for reaction in reactions:
		if reaction[0] == emoji:
			return reaction[1]
	return None

class InteractivePrompt(ControlledMessage):
	# Interactive prompts have different present behavior.
	# Returns True, if the keywords were bindable, and the prompt can be shown
	async def present(self):
		keywords = self.keywords()
		dontRegister = self.message != None or keywords == None or len(keywords) == 0
		
		if dontRegister or self.client.registerKeywords(keywords, self):
			await super().present(preEdit = True)
			return True
		
		return False

	def dismiss(self):
		keywords = self.keywords()
		if keywords != None and len(keywords) > 0:
			self.client.unregisterKeywords(keywords)

		super().dismiss()

	def sweepOnStart(self):
		return True

	def reactions(self):
		# pylint: disable=E1128
		# pylint: disable=E1133
		buttons = self.buttons()
		if buttons == None:
			buttons = []
		buttons = ["cancel"] + buttons
		return [reactionEmoji(name) for name in buttons]

	def keywords(self):
		return []

	async def onClick(self, emoji: str, user: User):
		reaction = reactionName(emoji)
		if reaction == None:
			return
		if reaction == "cancel":
			self.dismiss()
			return
		await self.handle(reaction, user)

	async def keyword(self, keyword, tokens, channel: Channel, user: User):
		if keyword == "dismiss":
			self.dismiss()
			return
		await self.handleResponse(keyword, tokens, user)

	def buttons(self):
		return None

	async def handle(self, button, user):
		pass

	async def handleResponse(self, keyword, tokens, user):
		pass

class MultiPagePrompt(InteractivePrompt):
	def __init__(self, client: BotClient, channel: Channel):
		super().__init__(client, channel)
		self.page = self.firstPage()

	async def update(self, page):
		self.page = page
		await super().update()

	def body(self):
		(body, _, _, _, _) = self.page
		return body

	def block(self):
		(_, block, _, _, _) = self.page
		return block

	def footer(self):
		(_, _, footer, _, _) = self.page
		return footer

	def buttons(self):
		(_, _, _, buttons, _) = self.page
		return buttons

	async def handle(self, reaction: str, user: User):
		(_, _, _, _, actions) = self.page
		if actions == None:
			actions = []
		if reaction in actions:
			await actions[reaction]()

	async def handleResponse(self, keyword: str, tokens: [str], user: User):
		(_, _, _, _, actions) = self.page
		if actions == None:
			actions = []
		if keyword in actions:
			await actions[keyword](tokens)

	def firstPage(self):
		return (None, None, None, None, None)
