from abc import ABC, abstractmethod
import discord
import asyncio
import traceback

from ..client import BotClient

usefulEmojis = "⚠️ 🔄 🇦 🇧 🇨 🇩 🇪 🇫 📝 🔒 🔓 🔧 ⬅ ➡ 👌 👍 👎 ♥ 💙 🆗 ☑ ✅ ❌ ❎ 🚫 ◀ ▶ ⏪ ⏩ 0⃣ 1⃣ 2⃣ 3⃣ 4⃣ 5⃣ 6⃣ 7⃣ 8⃣ 9⃣ 🔟"

class ControlledMessage(ABC):
	def __init__(self, client: BotClient, channel: discord.Channel):
		self.client = client
		self.channel = channel
		self.message = None
		self.appliedReactions = []
		pass

	async def present(self, preEdit = False):
		if self.message != None:
			await self.client.delete_message(self.message)
		
		content = self.messageContent()
		if preEdit:
			self.message = await self.client.send_message(self.channel, content)
			await self.client.edit_message(self.message, content)
		else:
			self.message = await self.client.send_message(self.channel, content)
		self.client.registerCom(self.message.id, self)
		await self.setReactions(self.reactions())

	async def update(self):
		content = self.messageContent()
		await self.client.edit_message(self.message, content)
		await self.setReactions(self.reactions())

	def dismiss(self):
		self.client.unregisterCom(self.message.id)
		async def deleteMessage():
			await self.client.delete_message(self.message)
		
		self.client.runCoroutine(deleteMessage)
	
	async def setReactions(self, reactions):
		visibleReactions = [] + self.appliedReactions
		desiredReactions = [] if reactions == None else [] + reactions

		# Ignore desired reactions which are already present, remove undesired visible reactions
		for _ in range(0, len(visibleReactions)):
			if len(desiredReactions) == 0:
				break
			emoji = visibleReactions[0]
			if visibleReactions[0] == desiredReactions[0]:
				desiredReactions = desiredReactions[1:]
				visibleReactions = visibleReactions[1:]
			else:
				await self.removeReaction(emoji)
				visibleReactions = visibleReactions[1:]
		
		# Remove any remaining visible reactions
		for n in range(0, len(visibleReactions)):
			emoji = visibleReactions[0]
			visibleReactions = visibleReactions[1:]
			await self.removeReaction(emoji)

		# Add any remaining desired reactions
		await self.addReactions(desiredReactions)

	async def addReactions(self, reactions):
		try:
			for emoji in reactions:
				await self.client.add_reaction(self.message, emoji)
				self.appliedReactions.append(emoji)
		except discord.errors.NotFound:
			print("Abort adding reactions to a non-existent message.")

	async def removeReaction(self, emoji, user=None):
		if user == None:
			user = self.client.user
		try:
			await self.client.remove_reaction(self.message, emoji, user)
			if user == self.client.user:
				self.appliedReactions.remove(emoji)
		except discord.errors.NotFound:
			print("Abort removing reaction from non-existent message.")

	def messageContent(self):
		# pylint: disable=E1128
		content = ""

		# Add optional header and tag to content
		tag = self.tag()
		header = self.header()
		if header != None:
			content += "\n**" + header + "**"
			if tag != None:
				content += "   `#" + ("{0:0=3d}".format(tag)) + "`"
			content += "\n"
		
		# Add body text to content
		body = self.body()
		block = self.block()
		if body != None:
			if header != None:
				content += "\n"
			content += body
			if block != None:
				content += "\n"

		# Add block text to content
		if block != None:
			content += "```fix\n"
			content += block + "\n"
			content += "\n```"

		# Add footer above reactions
		footer = self.footer()
		if footer != None:
			if block == None:
				content += "\n"
			content += "\n" + footer

		# Get meta info to encode at beginning
		typeId = self.client.encoding.typeIdForMessageObj(self)
		if tag == None:
			tag = 0
	
		return self.encodedContent(typeId, tag, content)

	def encodedContent(self, typeId: int, tag: int, content):
		return self.client.encoding.encodeContentString(
			self.sweepOnStart(), self.reinstantiateOnStart(),
			typeId, tag, content
		)
	
	async def reaction(self, emoji, user):
		#emoji = reaction.emoji

		#async def remove():
		#	await asyncio.sleep(0.2)
		#	await self.removeReaction(emoji, user)
		#self.client.runCoroutine(remove)

		await self.onClick(emoji, user)

	# ------------------------------
	# Behavioral Methods to Override
	# ------------------------------

	def sweepOnStart(self):
		return False

	def reinstantiateOnStart(self):
		return False

	def header(self):
		return None

	def tag(self):
		return None

	def body(self):
		return None

	def block(self):
		return None

	def footer(self):
		return None

	def reactions(self):
		return None

	def onClick(self, emoji, user):
		pass
