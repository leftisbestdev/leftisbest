import os
import bot.client
import bot.encoding
import bot.message.plain
import bot.poll.response

# Program Constants
hail = "~"
zero = "\u200B"
one = "\uFEFF"
term = "\u180E"
types = [
    bot.message.plain.ErrorMessage,
    bot.poll.response.PollResponsePrompt
]

token = os.environ.get("BOT_TOKEN")
encoding = bot.encoding.Encoding(zero, one, term, types)

bot.client.BotClient(hail, encoding).run(token)
