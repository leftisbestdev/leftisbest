import discord

class Encoding:
    def __init__(self, one, zero, term, types):
        self.oneEncoding = one
        self.zeroEncoding = zero
        self.terminator = term
        self.messageTypes = types

    def messageClassFromTypeId(self, typeId):
        if typeId < 1:
            return None
        return self.messageTypes[typeId - 1]

    def typeIdForMessageObj(self, obj):
        for n in range(len(self.messageTypes)):
            if isinstance(obj, self.messageTypes[n]):
                return n + 1
        return 0

    def binDigitEncode(self, digitStr):
        meta = digitStr.replace("1", self.oneEncoding)
        meta = meta.replace("0", self.zeroEncoding)
        return meta

    def binDigitDecode(self, meta):
        digitStr = meta.replace(self.oneEncoding, "1")
        digitStr = digitStr.replace(self.zeroEncoding, "0")
        return digitStr

    def encodeContentString(self, sweep: bool, reinstantiate: bool,
        typeId: int, tag: int, content: str):
        meta  = "1" if sweep else "0"
        meta += "1" if reinstantiate else "0"
        meta += "{:08b}".format(typeId)
        meta += "{:b}".format(tag)

        return self.binDigitEncode(meta) + self.terminator + content

    def decodeContentString(self, content: str):
        split = content.split(self.terminator)
        if len(split) != 2 or len(split[0]) < 11:
            return None
        
        meta = self.binDigitDecode(split[0])
        return({     
            "sweepOnStart" : meta[0] == "1",
            "reinstantiateOnStart" : meta[1] == "1",
            "typeId" : int(meta[2:10], 2),
            "tag" : int(meta[10:], 2),
            "content" : split[1]
        })
