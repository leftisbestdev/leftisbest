import re
import traceback
import discord
import asyncio

class BotClient(discord.Client):
	def __init__(self, hail, encoding):
		super().__init__()
		self.coms = {}
		self.keys = {}
		self.hail = hail
		self.encoding = encoding
		self.dispatcher = Dispatcher(self)
		
	def registerCom(self, id, com):
		self.coms[id] = com

	def unregisterCom(self, id):
		del self.coms[id]

	def registerKeywords(self, keywords, handler):
		for keyword in keywords:
			if keyword in self.keys:
				return False
		for keyword in keywords:
			self.keys[keyword] = handler
		return True

	def unregisterKeywords(self, keywords):
		for keyword in keywords:
			if keyword in self.keys:
				del self.keys[keyword]

	def runCoroutine(self, coro):
		loop = asyncio.get_event_loop()
		loop.create_task(coro())
	
	async def findPastMessagesInChannel(self, channel):
		messages = self.logs_from(channel)
		return [message async for message in messages if message.author == self.user]

	async def findPastMessages(self):
		messages = []

		for server in self.servers:
			for channel in server.channels:
				messages = messages + await self.findPastMessagesInChannel(channel)
		for channel in self.private_channels:
			messages = messages + await self.findPastMessagesInChannel(channel)

		return messages

	async def instantiate(self, message: discord.Message, typeId: int, tag: int):
		clazz = self.encoding.messageClassFromTypeId(typeId)
		if clazz != None:
			await clazz.instantiate(self, message, tag)

	async def on_ready(self):
		print("\nLogged in as " + self.user.name + " #" + self.user.id)

		prev = await self.findPastMessages()
		if len(prev) != 0:
			for message in prev:
				decoded = self.encoding.decodeContentString(message.content)
				if decoded == None or decoded["sweepOnStart"]:
					await self.delete_message(message)
				elif decoded["reinstantiateOnStart"]:
					await self.instantiate(message, decoded["typeId"], decoded["tag"])
		print("Ready")

	async def on_message(self, message):
		if self.sentToMe(message):
			tokens = self.tokens(message)
			await self.delete_message(message)

			if len(tokens) == 0:
				return

			key = tokens[0].lower()
			key = re.sub("-", "", key)
			key = re.sub("_", "", key)

			if "~" in self.keys:
				# Current implementation of greedy behavior needs to be modified
				# on a per-user basis
				action = self.keys["~"].keyword("~", tokens, message.channel, message.author)
				await self.tryAction(action, message.channel)
			elif key in self.keys:
				action = self.keys[key].keyword(key, tokens[1:], message.channel, message.author)
				await self.tryAction(action, message.channel)
			else:
				errorMessage = "Command `" + self.hail + key + "` is not defined."
				errorMessage += " Try using `~menu`."
				await EphemeralMessage(self, message.channel, errorMessage).present()

	async def on_reaction_add(self, reaction, user):
		if user == self.user:
			return
		emoji = reaction.emoji
		await self.remove_reaction(reaction.message, emoji, user)	
		if reaction.message.id in self.coms:
			action = self.coms[reaction.message.id].reaction(emoji, user)
			await self.tryAction(action, reaction.message.channel)
	
	async def tryAction(self, coro, channel):
		""" Used for most top-level async work, capturing and relaying most exceptions. """
		try:
			return await coro
		except Exception as ex:
			formattedLines = traceback.format_exc().splitlines()
			displayLines = formattedLines[-3:]
			ex = ""
			for line in displayLines:
				ex += line.strip() + "\n"
			
			await ErrorMessage(self, channel, ex).present()
			await self.close()
			raise

	def tokens(self, message):
		# Remove hail if present at start of line
		filtered = re.sub("^" + self.hail, "", message.content)

		# Remove any mentions of myself, treating mention as a whitepsace separator
		filtered = re.sub("<@![0-9]*>", " ", filtered)

		# Put whitespace around other mentions, ensuring they are individually tokenized
		filtered = re.sub("(<@[0-9]*>)", r" \1 ", filtered)

		return filtered.split()

	def sentToMe(self, message):
		if(message.author == self.user):
			return False
		
		channel = message.channel
		mentions = message.mentions
		if channel.is_private:
			return True
		if len(mentions) >= 1 and (self.user in mentions):
			return True

		if message.content.startswith(self.hail):
			return True

		return False

# Imported after class definition circumvents circular dependency issue
from .command import Dispatcher
from .message.plain import EphemeralMessage, ErrorMessage
