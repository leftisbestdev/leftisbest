import sys
import discord
import os
import traceback
import re

from .client import BotClient
from .message.plain import EphemeralMessage, ErrorMessage
from .poll.create import CreatePollWizard
from .poll.response import pollResponsePrompt
from .menu import MainMenu

class Dispatcher:
	def __init__(self, client):
		self.client = client
		self.table = {
			"hello": self.hello,
			"menu": self.menu,
			"poll": self.poll,
			"show": self.show,
			"boom": self.boom,
		}
		self.client.registerKeywords(list(self.table.keys()), self)

	async def keyword(self, key, tokens, channel, user) -> bool:
		await self.table[key](tokens, channel)

	async def hello(self, tokens: [str], channel: discord.Channel):
		await EphemeralMessage(self.client, channel, "Hello to you!").present()

	async def menu(self, tokens: [str], channel: discord.Channel):
		menu = MainMenu(self.client, channel)
		await menu.present()

	async def poll(self, tokens: [str], channel: discord.Channel):
		wizard = CreatePollWizard(self.client, channel)
		await wizard.present()

	async def show(self, tokens: [str], channel: discord.Channel):
		pollId = int(tokens[1])
		prompt = await pollResponsePrompt(self.client, channel, pollId)
		await prompt.present()

	async def boom(self, tokens: [str], channel: discord.Channel):
		1/0
