<?php

if($_SERVER['REQUEST_METHOD'] !== 'POST') {

?>

<html>
    <body>
        <form action="/backend/get-preview" method="post">
            Web Address <input type="text" name="url"> <input type="submit">
        </form>
    </body>
</html>

<?php
    exit;
}

require('meta-parser.php');

function getData($url) {
	// create curl resource
    $ch = curl_init();

    // set url
    curl_setopt($ch, CURLOPT_URL, $url);

    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    // $output contains the output string
    $output = curl_exec($ch);

    // close curl resource to free up system resources
    curl_close($ch);   

    return $output;
}

$url = $_POST['url'];
$body = getData($url);

error_reporting(error_reporting() & ~E_NOTICE);
$parser = new MetaParser($body, $url);
$result = $parser->getDetails();

header('Content-type: application/json');
echo json_encode($result);
