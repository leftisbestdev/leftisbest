#!/bin/bash

# Repository should already be recursively owned by ec2-user:www-user,
# and gid bit set on directories so new files are also ec2-user:www-user

# Web server group (www-user) can not write, only ec2-user
# This permission is already on everything in home & www, except wp-content
umask 137

# Kill bot process and Apache if running
bot_pid=$(pgrep -f "python3 -m bot")
if [ ! -z "$bot_pid" ]; then
	kill ${bot_pid}
else
	echo "Bot not running"
fi
sudo service httpd stop

# Move to repository and pull latest code
cd ~/leftisbest
git pull

# Move non-hidden files at top level to www
cp ./* ~/www/

# Move contents of plugins & themes into active wordpress installation
rm -rf ~/www/html/wp-content/plugins/lib-plugins
rm -rf ~www/html/wp-content/themes/lib-themes
cp -r ./plugins ~/www/html/wp-content/plugins/lib-plugins
cp -r ./themes ~/www/html/wp-content/themes/lib-themes

# Move miscellaneous non-wordpress code to www/html/backend
rm -rf ~/www/html/backend
cp -r ./backend ~/www/html/backend

# Move bot code to home/bot
rm -rf ~/bot
cp -r ./bot ~/bot

# Move to www and install any new PHP dependencies
cd ~/www
composer install

# Move back home and restart the bot and Apache
cd ~
python3 -m bot &
sudo service httpd start
