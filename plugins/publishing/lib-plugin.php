<?php

class LeftIsBestPlugin
{
    protected function shortcodes($dictionary) {
        foreach ($dictionary as $name => $callback)
            add_shortcode($name, [$this, $callback]);
    }

    protected function shortcode($name, $callback) {
        add_shortcode($name, [$this, $callback]);
    }

    protected function actions($dictionary) {
        foreach ($dictionary as $name => $callback)
            add_action($name, [$this, $callback]);
    }

    protected function filters($dictionary) {

    }

    protected function url($filename) {
        echo plugins_url($filename, __FILE__);
    }

    protected function template($name, $parameters=null) {
        ob_start();
        $this->echoTemplate($name, $parameters);
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

    protected function echoTemplate($name, $parameters=null) {
        if ($parameters == null)
            $parameters = [];
        
        require('templates/' . $name . '.php');
    }

    protected function requireScript($name) {
        return wp_enqueue_script(
            $name . '-js',
            plugins_url('js/' . $name . '.js', __FILE__),
            ['jquery'], null, true
        );
    }
}
