<?php

require_once(dirname(__FILE__) . '/lib-plugin.php');
require_once(dirname(__FILE__) . '/publishing-session.php');

class PublishingForm extends LeftIsBestPlugin
{
    public function __construct(PublishingSession $session) {
        $this->shortcodes([
            'publish-page' => 'renderPublishPage'
        ]);
        $this->actions([
            'wp_enqueue_scripts' => 'loadPluginCSS'
        ]);
        $this->filters([

        ]);

        $this->session = $session;
    }

    public function loadPluginCSS() {
        $stylesheet = plugins_url('css/default.css', __FILE__);
        wp_enqueue_style('style1', $stylesheet);
    }

    public function authorForNewPost() {
        if (is_user_logged_in())
            return wp_get_current_user();
        
        return get_user_by('login', 'fakeuser');
    }

    public function publish() {
        $author = $this->authorForNewPost();

        $postarr = [
            'ID' => 0,
            'post_author' => $author->ID,
            'post_content' => $_POST['post-content'],
            'post_title' => $_POST['post-title'],
            'post_status' => 'publish',
            'post_type' => 'post'

        ];

        return wp_insert_post($postarr, true);
    }

    public function renderPublishPage() {
        if (isset($_REQUEST['reset']))
            $this->session->reset();
        
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // Wordpress adds slashes for some reason
            $_POST = array_map('stripslashes_deep', $_POST);

            // Save field values to be repopulated if we return to the form
            $this->session->publishFormUpdate();

            /*$result = $this->publish();

            if (is_wp_error($result))
                var_dump($result);
            else
                echo '<h1>Success</h1>';*/
        }

        $this->requireScript("publish-page");
        return $this->template('publish-page', [
            'form-state' => $this->session->publishFormState()
        ]);
    }
}
