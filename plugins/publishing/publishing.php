<?php
/**
 * Plugin Name: Publishing
 * Description: Allow authenticated users to publish through website frontend
 * Version:     2-March-2019
 * Author:      LeftIsBest Development
 * License:     Private
 */

require_once getenv("APP_ROOT").'/../vendor/autoload.php';

require_once (dirname(__FILE__) . '/publishing-session.php');
require_once (dirname(__FILE__) . '/publishing-form.php');
require_once (dirname(__FILE__) . '/publishing-auth.php');

$dotenv = Dotenv\Dotenv::create(getenv("APP_ROOT").'/../');
$dotenv->load();

$libpubSession = new PublishingSession();

new PublishingForm($libpubSession);
new PublishingAuth($libpubSession);
