<?php

class PublishingSession
{
    public function __construct() {
        if (session_status() == PHP_SESSION_NONE)
            session_start();
    }

    public function reset() {
        unset($_SESSION['libpub-form-state']);
        unset($_SESSION['libpub-auth-state']);
    }

    public function authState() {
        if (!isset($_SESSION['libpub-auth-state']))
            $_SESSION['libpub-auth-state'] = $this->initAuthState();

        return $_SESSION['libpub-auth-state'];
    }

    public function authStart($outlet, $target, $oauthState) {
        $state = $this->authState();

        $state['outlet'] = $outlet;
        $state['target'] = $target;
        $state['oauthState'] = $oauthState;

        if($outlet == 'discord')
            $state['discordToken'] = null;
        elseif($outlet == 'patreon')
            $state['patreonToken'] = null;

        $_SESSION['libpub-auth-state'] = $state;
    }

    public function authStateDoesMatch() {
        echo '[' . $_GET['state'] . ']\n[' . $this->authState()['oauthState'] . ']';
        return $_GET['state'] === $this->authState()['oauthState'];
    }

    public function authGranted($token) {
        return $this->authComplete($token);
    }

    public function authFailed() {
        return $this->authComplete(null);
    }

    private function authComplete($tokenOrNull=null) {
        $state = $this->authState();

        if($state['outlet'] == 'discord')
            $state['discordToken'] = $tokenOrNull;
        elseif($state['outlet'] == 'patreon')
            $state['patreonToken'] = $tokenOrNull;

        $target = $state['target'];

        $state['target'] = null;
        $state['outlet'] = null;

        $_SESSION['libpub-auth-state'] = $state;

        return $target;
    }

    private function initAuthState($initDiscordToken=null, $initPatreonToken=null) {
        return [
            'outlet' => null,
            'target' => null,
            'oauthState' => null,
            'discordToken' => $initDiscordToken,
            'patreonToken' => $initPatreonToken
        ];
    }

    public function publishFormFinished() {
        unset($_SESSION['libpub-form-state']);
    }

    public function publishFormState() {
        if (!isset($_SESSION['libpub-form-state']))
            $_SESSION['libpub-form-state'] = $this->initPublishFormState();

        return $_SESSION['libpub-form-state'];
    }

    public function publishFormUpdate() {
        $state = $this->publishFormState();

        $textFields = [
            'selected-form',
            'link-url', 'link-author', 'link-author-name',
            'post-title', 'post-content',
            'tweet-url',
            'video-url', 'video-author', 'video-author-name',
            'resource-url', 'resource-contact', 'resource-contact-name'
        ];
        $booleanFields = [
            'link-long-form'
        ];

        foreach ($textFields as $fieldName)
            $state[$fieldName] = $_POST[$fieldName];

        foreach ($booleanFields as $fieldName)
            $state[$fieldName] = isset($_POST[$fieldName]);

        // Fields id, verification are unchanged
        $_SESSION['libpub-form-state'] = $state;
    }

    public function initPublishFormVerification() {
        // Verified from the start if user is logged in and verified...
        return null;
    }

    private function initPublishFormState() {
        return [
            'id' => mt_rand(),
            'verification' => $this->initPublishFormVerification(),
            'selected-form' => 'post',
            
            'link-url' => '',
            'link-long-form' => false,
            'link-author' => 'self',
            'link-author-name' => '',
            
            'post-title' => '',
            'post-content' => '',
            
            'tweet-url' => '',
            
            'video-url' => '',
            'video-author' => 'self',
            'video-author-name' => '',
            
            'resource-url' => '',
            'resource-contact' =>  '',
            'resource-contact-name' => ''
        ];
    }
}
