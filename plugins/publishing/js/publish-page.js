jQuery(document).ready(function($) {
	

$('ul.libpub-tabs li').click(function(){
	var formSelection = $(this).attr('data-tab');
	
	$('.libpub-tab').removeClass('selected');
	$('.libpub-form-body').removeClass('selected');

	$(this).addClass('selected');
	$("#libpub-" + formSelection + "-form-body").addClass('selected');
	$("#libpub-selected-form").val(formSelection);
})

$('#libpub-verify-patreon').click(function() {
	window.open('/backend/redirect-patreon', "", "width=600,height=800");
});

$('#libpub-verify-discord').click(function() {
	window.open('/backend/redirect-discord', "", "width=600,height=800");
});


});
