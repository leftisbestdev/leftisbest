<?php 
    $values = $parameters['form-state'];

    $ev = function($identifier) use ($values) {
        echo htmlspecialchars($values[$identifier]);
    };

    $eif = function($identifier, $ifTrue = '', $ifFalse = '') use ($values)  {
        echo $values[$identifier] ? $ifTrue : $ifFalse;
    };

    $eifneq = function($identifier, $expected, $ifTrue = '', $ifFalse = '') use ($values) {
        echo $values[$identifier] != $expected ? $ifTrue : $ifFalse;
    };

    $eifeq = function($identifier, $expected, $ifTrue = '', $ifFalse = '') use ($values)  {
        echo $values[$identifier] == $expected ? $ifTrue: $ifFalse;
    };
?>


<!-- Post Type Selection Tabs -->
<div class="libpub-form-section">
    <p>
        Members of the <a href="/community">community</a> can post links, long-form writing, tweets, videos, or informational resources to the site.
    </p>

    <ul class="libpub-tabs">
        <li data-tab="link"
            class="libpub-tab <?php $eifeq('selected-form', 'link', 'selected'); ?>">
            <img src="<?php $this->url('img/link-light.png'); ?>">
        </button>

        <li data-tab="post"
            class="libpub-tab <?php $eifeq('selected-form', 'post', 'selected'); ?>">
            <img src="<?php $this->url('img/write-light.png'); ?>">
        </button>

        <li data-tab="tweet"
            class="libpub-tab <?php $eifeq('selected-form', 'tweet', 'selected'); ?>">
            <img src="<?php $this->url('img/twitter-light.png'); ?>">
        </button>

        <li data-tab="video"
            class="libpub-tab <?php $eifeq('selected-form', 'video', 'selected'); ?>">
            <img src="<?php $this->url('img/youtube-light.png'); ?>">
        </button>

        <li data-tab="resource"
            class="libpub-tab <?php $eifeq('selected-form', 'resource', 'selected'); ?>">
            <img src="<?php $this->url('img/information-light.png'); ?>">
        </button>
    </ul>
</div>

<form id="libpub-form" method="post" enctype="multipart/form-data">

    <input type="hidden"
        id="libpub-selected-form"
        name="selected-form"
        value="<?php $ev('selected-form') ?>">

    <hr>

    <!-- Link Submission Form -->
    <div id="libpub-link-form-body"
        class="libpub-form-body <?php $eifeq('selected-form', 'link', 'selected'); ?>">

        <div class="libpub-form-section">
            <input type="text"
                placeholder="Link Address"
                name="link-url"
                value="<?php $ev('link-url'); ?>">
        </div>

        <div class="libpub-form-section">
            <input type="checkbox"
                name="link-long-form"
                <?php $eif('link-long-form', 'checked'); ?>>
            <label>This is an article or long-form post</label>
        </div>

        <div class="libpub-form-section">
            <div>
                <input type="radio"
                    name="link-author"
                    value="self"
                    <?php $eifeq('link-author', 'self', 'checked') ?>>
                <label>My original content</label>
            </div>
            <div>
                <input type="radio"
                    name="link-author"
                    value="other"
                    <?php $eifeq('link-author', 'other', 'checked') ?>>
                <label>
                    Created by
                    <input type="text"
                        placeholder="someone else"
                        name="link-author-name"
                        value="<?php $ev('link-author-name'); ?>">
                </label>
            </div>
        </div>

    </div>

    <!-- Post Submission Form -->
    <div id="libpub-post-form-body"
        class="libpub-form-body <?php $eifeq('selected-form', 'post', 'selected'); ?>">

        <div class="libpub-form-section">
            <input type="text"
                placeholder="Title"
                name="post-title"
                value="<?php $ev('post-title'); ?>">
        </div>

        <div class="libpub-form-section">
            <?php wp_editor($values['post-content'], "post-content"); ?>
        </div>

        <div class="libpub-form-section">
            <p>Featured Image</p>
            <input type="file" name="post-image">
        </div>
    </div>

    <!-- Tweet Submission Form -->
    <div id="libpub-tweet-form-body"
        class="libpub-form-body <?php $eifeq('selected-form', 'tweet', 'selected'); ?>">

        <div class="libpub-form-section">
            <input type="text"
                placeholder="Twitter URL"
                name="tweet-url"
                value="<?php $ev('tweet-url'); ?>">
        </div>
    </div>

    <!-- Youtube Submission Form -->
    <div id="libpub-video-form-body"
        class="libpub-form-body <?php $eifeq('selected-form', 'video', 'selected'); ?>">

        <div class="libpub-form-section">
            <input type="text"
                placeholder="YouTube URL"
                name="video-url"
                value="<?php $ev('video-url'); ?>">
        </div>

        <div class="libpub-form-section">
            <div>
                <input type="radio"
                    name="video-author"
                    value="self"
                    <?php $eifeq('video-author', 'self', 'checked'); ?>>
                <label>My original content</label>
            </div>
            <div>
                <input type="radio"
                    name="video-author"
                    value="other"
                    <?php $eifeq('video-author', 'other', 'checked'); ?>>
                <label>
                    Created by
                    <input type="text"
                        placeholder="someone else"
                        name="video-author-name"
                        value="<?php $ev('video-author-name'); ?>">
                </label>
            </div>
        </div>
    </div>

    <!-- Resource Submission Form -->
    <div id="libpub-resource-form-body"
        class="libpub-form-body <?php $eifeq('selected-form', 'resource', 'selected'); ?>">

        <div class="libpub-form-section">
            <input type="text"
                placeholder="Resource URL"
                name="resource-url"
                value="<?php $ev('resource-url'); ?>">
        </div>

        <div class="libpub-form-section">
            <p>
                Resources submissions need to be reviewed before they are added to the site.
            </p>
            <p>
                Who should we contact if there are any questions?
            </p>
        </div>

        <div class="libpub-form-section">
            Talk to
            <input type="text"
                placeholder="somebody"
                name="resource-contact-name"
                value="<?php $ev('resource-contact-name'); ?>">
            at
            <input type="text"
                placeholder="an email, phone #, etc"
                name="resource-contact"
                value="<?php $ev('resource-contact'); ?>">
        </div>
    </div>

    <hr>

    <!-- Verification & Submitter -->
    <div class="libpub-form-section">

        <span class="libpub-auth-region <?php $eifneq('verification', 'patreon', 'hidden'); ?>">
            <img class="libpub-verify-icon" src="<?php $this->url('img/patreon-verified.png'); ?>">
        </span>

        <span class="libpub-auth-region <?php $eifneq('verification', 'discord', 'hidden'); ?>">
            <img class="libpub-verify-icon" src="<?php $this->url('img/discord-verified.png'); ?>">
        </span>

        <span class="libpub-auth-region <?php $eifneq('verification', null, 'hidden'); ?>">
            <button id="libpub-verify-patreon">
                <img class="libpub-verify-icon" src="<?php $this->url('img/patreon-icon.png'); ?>">
            </button>

            <button id="libpub-verify-discord">
                <img class="libpub-verify-icon" src="<?php $this->url('img/discord-icon.png'); ?>">
            </button>
        </span>

        <input type="text"
            placeholder="<?php $eifeq('verification', null, 'Verify Submitter', 'Display Name') ?>"
            name="submitter"
            <?php $eifeq('verification', null, 'disabled'); ?>>

        <?php if (!is_user_logged_in()) : ?>

        <label>
            or, <a href="sign-in">Sign In</a>
        </label>

        <?php endif; ?>
    </div>

    <hr>

    <!-- Submit & Register -->
    <div class="libpub-form-section">
        <button>Submit</button>
        <input type="checkbox" name="register">
        <label>Also register an account at LeftIsBest.org</label>
    </div>

</form>
