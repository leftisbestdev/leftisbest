@echo off

setlocal

set repo=%~dp0
set repo=%repo:~0,-1%

if ["%~1"]==[""] (
    set wamp=C:\wamp64
) else (
    set wamp=%1
)

echo.
echo Running composer...
call composer install

echo.
echo Copying files...
copy /Y %repo%\script-pack\wp-config.php %wamp%\www\wp-config.php
copy /Y %repo%\script-pack\template.htaccess %wamp%\www\.htaccess
if exist %wamp%\.env (
    echo Skipping .env since it already exists
) else (
    copy /Y %repo%\script-pack\template.env %wamp%\.env
)

echo.
echo Deleting old symlinks...

fsutil reparsepoint query %wamp%\vendor | find "Symbolic Link" >nul && (
    echo %wamp%\vendor
    rmdir /S /Q %wamp%\vendor
)
fsutil reparsepoint query %wamp%\www\backend | find "Symbolic Link" >nul && (
    echo %wamp%\www\backend
    rmdir /S /Q %wamp%\www\backend
)
for /D %%s in (%wamp%\www\wp-content\plugins\*) do (
    fsutil reparsepoint query %%s | find "Symbolic Link" >nul && (
        echo %%s
        rmdir /S /Q %%s
    )
)
for /D %%s in (%wamp%\www\wp-content\themes\*) do (
    fsutil reparsepoint query %%s | find "Symbolic Link" >nul && (
        echo %%s
        rmdir /S /Q %%s
    )
)

echo.
echo Making new symlinks...

mklink /D %wamp%\vendor %repo%\vendor
mklink /D %wamp%\www\backend %repo%\backend
for /D %%s in (%repo%\plugins\*) do (
    mklink /D %wamp%\www\wp-content\plugins\%%~nxs %%s
)
for /D %%s in (%repo%\themes\*) do (
    mklink /D %wamp%\www\wp-content\themes\%%~nxs %%s
)
